<section class="mt-40">
    <h2 class="section-title"><?php echo e(trans('public.history_request')); ?></h2>

    <div class="panel-section-card py-20 px-25 mt-20">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table text-center custom-table">
                        <thead>
                            <tr>
                                <th class="text-center font-weight-bold"><?php echo e(trans('#')); ?></th>
                                <th class="text-center font-weight-bold"><?php echo e(trans('public.name')); ?></th>
                                <th class="text-center font-weight-bold"><?php echo e(trans('public.email')); ?></th>
                                <th class="text-center font-weight-bold"><?php echo e(trans('public.amount')); ?></th>
                                <th class="text-center font-weight-bold"><?php echo e(trans('public.date_of_payment')); ?></th>
                                <th class="text-center font-weight-bold"><?php echo e(trans('public.status')); ?></th>
                                <th class="text-center font-weight-bold"><?php echo e(trans('public.action')); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $requestPayment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-center align-middle">
                                        <span class="font-weight"><?php echo e($item->id); ?></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span class="font-weight"><?php echo e($item->name); ?></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span class="font-weight"><?php echo e($item->email); ?></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span class="font-weight"><?php echo e($item->amount); ?></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span class="font-weight"><?php echo e(\Carbon\Carbon::parse($item->date_of_payment)->format('d/m/Y')); ?></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <?php switch($item->status):
                                            case (\App\Models\RequestPayment::$waiting): ?>
                                                <span class="text-warning"><?php echo e(trans('public.waiting')); ?></span>
                                                <?php break; ?>
                                            <?php case (\App\Models\RequestPayment::$approved): ?>
                                                <span class="text-primary"><?php echo e(trans('financial.approved')); ?></span>
                                                <?php break; ?>
                                            <?php case (\App\Models\RequestPayment::$reject): ?>
                                                <span class="text-danger"><?php echo e(trans('public.cancel')); ?></span>
                                                <?php break; ?>
                                        <?php endswitch; ?>
                                    </td>
                                    <td class="align-middle">
                                        <form action="/panel/financial/account/cancel/<?php echo e($item->id); ?>" method="POST">
                                            <?php echo csrf_field(); ?>
                                            <input type="hidden" name="status" value="2">
                                            <?php switch($item->status):
                                                case (\App\Models\RequestPayment::$waiting): ?>
                                                    <button type="submit" class="text-center align-middle btn btn-sm btn-danger"><?php echo e(trans('public.cancel')); ?></button>
                                                    <?php break; ?>
                                                <?php case (\App\Models\RequestPayment::$approved): ?>
                                                <?php case (\App\Models\RequestPayment::$reject): ?>
                                                    <button type="submit" disabled class="text-center align-middle btn btn-sm btn-danger"><?php echo e(trans('public.cancel')); ?></button>
                                                    <?php break; ?>
                                            <?php endswitch; ?>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <div class="mt-20">
                    <?php echo e($requestPayment->links()); ?>

                </div>
            </div>
        </div>
    </div>
</section><?php /**PATH /opt/lampp/htdocs/newj.tq18.tech/public_html/resources/views/web/default/includes/account/historyRequest.blade.php ENDPATH**/ ?>