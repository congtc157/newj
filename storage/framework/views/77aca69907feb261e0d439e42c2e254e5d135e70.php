<?php $__env->startSection('content'); ?>
    <section class="section">
        <div class="section-header">
            <h1><?php echo e(trans('admin/main.new_document')); ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo e(getAdminPanelUrl()); ?>"><?php echo e(trans('admin/main.dashboard')); ?></a>
                </div>
                <div class="breadcrumb-item"><?php echo e(trans('admin/main.new_document')); ?></div>
            </div>
        </div>

        <div class="section-body card">
            <div class="row">
                <div class="col-12 col-md-4 pr-0">
                    <div class="card-body">
                        <form action="<?php echo e(getAdminPanelUrl()); ?>/financial/documents/store" method="post">
                            <?php echo e(csrf_field()); ?>


                            <?php if(!empty($currencies) and count($currencies)): ?>
                                <div class="form-group">
                                    <label class="input-label d-block"><?php echo e(trans('admin/main.currency')); ?></label>
                                    <select name="currency" class="js-ajax-currency form-control select2"
                                        data-placeholder="<?php echo e(trans('admin/main.currency')); ?>">
                                        <option value=""></option>
                                        <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currencyItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($currencyItem->currency); ?>"
                                                <?php echo e(currency() == $currencyItem->currency ? 'selected' : ''); ?>>
                                                <?php echo e(currenciesLists($currencyItem->currency)); ?>

                                                (<?php echo e(currencySign($currencyItem->currency)); ?>)
                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>

                                    <?php $__errorArgs = ['currency'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <div class="invalid-feedback d-block"><?php echo e($message); ?></div>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="currency" value="<?php echo e(getDefaultCurrency()); ?>">
                            <?php endif; ?>

                            <div class="form-group">
                                <label class="control-label"><?php echo e(trans('admin/main.amount')); ?>

                                    (<?php echo e(currencySign(getDefaultCurrency())); ?>)</label>
                                <input type="text" name="amount"
                                    class="form-control <?php $__errorArgs = ['amount'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">

                                <?php $__errorArgs = ['amount'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback">
                                        <?php echo e($message); ?>

                                    </div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>

                            <div class="form-group">
                                <label class="input-label d-block"><?php echo e(trans('admin/main.type')); ?></label>
                                <select name="type" class="form-control <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                                    <option value="addiction"><?php echo e(trans('admin/main.addiction')); ?></option>
                                    <option value="deduction"><?php echo e(trans('admin/main.deduction')); ?></option>
                                </select>

                                <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback">
                                        <?php echo e($message); ?>

                                    </div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>

                            <div class="form-group">
                                <label class="input-label d-block"><?php echo e(trans('admin/main.user')); ?></label>
                                <select name="user_id"
                                    class="form-control search-user-select2 <?php $__errorArgs = ['user_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                                    data-placeholder="<?php echo e(trans('public.search_user')); ?>">

                                </select>

                                <?php $__errorArgs = ['user_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback">
                                        <?php echo e($message); ?>

                                    </div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>

                            <div class="form-group">
                                <label class="control-label"><?php echo e(trans('admin/main.description')); ?></label>
                                <textarea name="description" class="form-control <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" rows="6"></textarea>
                                <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback">
                                        <?php echo e($message); ?>

                                    </div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>

                            <button type="submit" class="btn btn-primary"><?php echo e(trans('admin/main.submit')); ?></button>
                        </form>
                    </div>
                </div>
                <?php echo $__env->make('admin.financial.documents.historyRequest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.aizzi.me/aizzi.js" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
      $('select[name="action"]').change(function() {
        var selectedValue = $(this).val();
        var itemId = $(this).data('item-id');
        console.log(selectedValue);
        
        // Thực hiện cuộc gọi Ajax tại đây với giá trị đã chọn
        $.ajax({
          url: '/admin/financial/documents/status/' + itemId,
          method: 'POST',
          data: { value: selectedValue },
          success: function(response) {
            
            window.location.reload();
          },
          error: function(xhr, status, error) {
          }
        });
      });
    });
  </script>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /opt/lampp/htdocs/newj.tq18.tech/public_html/resources/views/admin/financial/documents/new.blade.php ENDPATH**/ ?>