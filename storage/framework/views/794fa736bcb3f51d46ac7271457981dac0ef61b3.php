<section class="mt-40">
    <h2 class="section-title"><?php echo e(trans('financial.offline_transactions_history')); ?></h2>

    <div class="panel-section-card py-20 px-25 mt-20">
        <div class="row">
            <div class="col-12 ">
                <div class="table-responsive">
                    <table class="table text-center custom-table">
                        <thead>
                            <tr>
                                <th><?php echo e(trans('financial.bank')); ?></th>
                                <th><?php echo e(trans('admin/main.referral_code')); ?></th>
                                <th class="text-center"><?php echo e(trans('panel.amount')); ?> (<?php echo e($currency); ?>)</th>
                                <th class="text-center"><?php echo e(trans('update.attachment')); ?></th>
                                <th class="text-center"><?php echo e(trans('public.status')); ?></th>
                                <th class="text-right"><?php echo e(trans('public.controls')); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $offlinePayments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offlinePayment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-left">
                                        <div class="d-flex flex-column">

                                            <?php if(!empty($offlinePayment->offlineBank)): ?>
                                                <span
                                                    class="font-weight-500 text-dark-blue"><?php echo e($offlinePayment->offlineBank->title); ?></span>
                                            <?php else: ?>
                                                <span class="font-weight-500 text-dark-blue">-</span>
                                            <?php endif; ?>
                                            <span
                                                class="font-12 text-gray"><?php echo e(dateTimeFormat($offlinePayment->pay_date, 'j M Y H:i')); ?></span>
                                        </div>
                                    </td>
                                    <td class="text-left align-middle">
                                        <span><?php echo e($offlinePayment->reference_number); ?></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span
                                            class="font-16 font-weight-bold text-primary"><?php echo e(handlePrice($offlinePayment->amount, false)); ?></span>
                                    </td>

                                    <td class="text-center align-middle">
                                        <?php if(!empty($offlinePayment->attachment)): ?>
                                            <a href="<?php echo e($offlinePayment->getAttachmentPath()); ?>" target="_blank"
                                                class="text-primary"><?php echo e(trans('public.view')); ?></a>
                                        <?php else: ?>
                                            ---
                                        <?php endif; ?>
                                    </td>

                                    <td class="text-center align-middle">
                                        <?php switch($offlinePayment->status):
                                            case (\App\Models\OfflinePayment::$waiting): ?>
                                                <span class="text-warning"><?php echo e(trans('public.waiting')); ?></span>
                                            <?php break; ?>

                                            <?php case (\App\Models\OfflinePayment::$approved): ?>
                                                <span class="text-primary"><?php echo e(trans('financial.approved')); ?></span>
                                            <?php break; ?>

                                            <?php case (\App\Models\OfflinePayment::$reject): ?>
                                                <span class="text-danger"><?php echo e(trans('public.rejected')); ?></span>
                                            <?php break; ?>
                                        <?php endswitch; ?>
                                    </td>
                                    <td class="text-right align-middle">
                                        <?php if($offlinePayment->status != 'approved'): ?>
                                            <div class="btn-group dropdown table-actions">
                                                <button type="button" class="btn-transparent dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <i data-feather="more-vertical" height="20"></i>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a href="/panel/financial/offline-payments/<?php echo e($offlinePayment->id); ?>/edit"
                                                        class="webinar-actions d-block mt-10"><?php echo e(trans('public.edit')); ?></a>
                                                    <a href="/panel/financial/offline-payments/<?php echo e($offlinePayment->id); ?>/delete"
                                                        data-item-id="1"
                                                        class="webinar-actions d-block mt-10 delete-action"><?php echo e(trans('public.delete')); ?></a>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section><?php /**PATH /opt/lampp/htdocs/newj.tq18.tech/public_html/resources/views/web/default/includes/account/offlinePayment.blade.php ENDPATH**/ ?>